#!/bin/bash
prev_n=26
for n in {27..36}
do
		
	echo "#!/bin/bash" > $prev_n/solution
	echo "" >> $prev_n/solution
	echo "solution=\"\"" >> $prev_n/solution
	echo "" >> $prev_n/solution
	echo "# Do commands here like login to specific machine " >> $prev_n/solution
	echo "# and then save result to \$solution" >> $prev_n/solution
	echo "# that will be directed to save into pass file upon execution" >> $prev_n/solution
	echo "" >> $prev_n/solution
	echo "# example login" >> $prev_n/solution
	echo "" >> $prev_n/solution
	echo "cd ~kris/bandit/" >> $prev_n/solution
	echo "solution=\$(./login.sh $prev_n)" >> $prev_n/solution
	echo "" >> $prev_n/solution
	echo "" >> $prev_n/solution
	echo "echo bandit$n:\$solution > ~kris/bandit/$n/pass" >> $prev_n/solution
	
	prev_n=$n
	chmod +x $n/solution

done
