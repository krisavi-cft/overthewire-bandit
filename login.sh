#!/bin/bash


level="$1"

IFS='
'
file=( $( < $level/pass ) )
IFS=':'
read -r -a array <<< "$file"

user=${array[0]}
pass=${array[1]}

sshpass -p "$pass" ssh $user@bandit.labs.overthewire.org -p 2220 "${@:2}"
#ssh bandit0@bandit.labs.overthewire.org -p 2220
